import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreerUserPage } from './creer-user.page';

describe('CreerUserPage', () => {
  let component: CreerUserPage;
  let fixture: ComponentFixture<CreerUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreerUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreerUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
